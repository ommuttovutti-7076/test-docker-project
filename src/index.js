const Koa = require('koa')
const app = new Koa();

app
  .use((ctx, next) => {
    ctx.response.body = 'Hello World!'
    next()
  })
const PORT = process.env.SERVER_PORT || 24713

const server = app.listen(PORT, () => console.log(`Server listening on port ${PORT}`))
