FROM node:11-alpine
WORKDIR /app
COPY . /app
RUN npm install
CMD ["./node_modules/.bin/pm2-runtime", "src/index.js"]